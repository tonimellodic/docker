# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://docs.docker.com/engine/reference/builder/#from
FROM debian:buster-slim

# https://gitlab.com/gitlab-org/gitlab-runner/issues/1170
RUN echo dash dash/sh boolean false | debconf-set-selections  \
&&  mkdir -p /usr/share/man/man1  \
&&  dpkg-reconfigure -f noninteractive dash

# https://docs.docker.com/engine/reference/builder/#run
RUN apt-get update  \
&&  apt-get install -y python-pip

# https://pip.pypa.io/en/stable/reference/pip_install/
RUN pip install yamllint==1.14.0

# https://yamllint.readthedocs.io/en/v1.14.0/configuration.html
ADD default-config.yml /root/.config/yamllint/config

# https://docs.docker.com/engine/reference/builder/#cmd
CMD ["/bin/bash"]
