#!/bin/bash
# This is the main entrypoint into the container. Its responsible for
# copying some helper scripts to the users build directory and then
# starting/stopping web page replay
set -e

OUTPUT_DIR=${OUTPUT_DIR:-$CI_PROJECT_DIR}

# Wait for outputdir to appear - it can take a few seconds
while [ ! -d "${OUTPUT_DIR}" ] && [ "${waittime:-0}" -lt 60 ]
do
    echo "Waiting for ${OUTPUT_DIR} to appear..."
    ((waittime=waittime+1))
    sleep 1
done

if [ ! -d "${OUTPUT_DIR}" ]
then
    echo "\${OUTPUT_DIR}(${OUTPUT_DIR}) doesnt exist! Exiting."
    exit 1
fi

if [ -z "${WPR_FILE}" ]
then
    echo "\${WPR_FILE} needs to be set to the to be used by wpr. Exiting."
    exit 1
fi

# Copy our wpr_scripts and TLS files to a folder
# for the user to run from the main container.
# Note it may already exist since its excluded from the git clean
rm -fr "${OUTPUT_DIR}"/wpr_scripts/
cp -r /wpr_scripts/ "${OUTPUT_DIR}"
cp /go/catapult/web_page_replay_go/wpr_* "${OUTPUT_DIR}"/wpr_scripts/

# Avoid a potential race condition when the file is still being checked out
while [ ! -f "${OUTPUT_DIR}/${WPR_FILE}" ] && [ "${waittime:-0}" -lt 180 ]
do
    echo "Waiting for ${WPR_FILE} to appear..."
    ((waittime=waittime+1))
    sleep 1
done


# Figure out if we want to record a new file or replay an existing one
if [ -f "${OUTPUT_DIR}/${WPR_FILE}" ] ; then
    WPR_MODE="replay"
    # Wait for the file to finish growing
    while [ "$(stat -c %b "${OUTPUT_DIR}/${WPR_FILE}")" -ne "${size:-1}" ]
        do echo "Waiting for WPR file to complete checkout"
        size=$(stat -c %b "${OUTPUT_DIR}/${WPR_FILE}")
        sleep 5
    done
    WPR_FILE="${OUTPUT_DIR}/${WPR_FILE}"
    echo "Enabling ${WPR_MODE} mode using existing WPR file: ${WPR_FILE}"
else
    WPR_MODE="record"
    # Write the new wpr file to outside OUTPUT_DIR so it doesnt
    # get clobbered by the git repo init (it gets moved later)
    NEW_WPR_FILE="${OUTPUT_DIR}/${WPR_FILE}"
    WPR_FILE="/go/${WPR_FILE}"
    echo "Enabling ${WPR_MODE} mode using new WPR file: ${WPR_FILE}"
fi

# Start wpr
go run src/wpr.go ${WPR_MODE} \
    --host=0.0.0.0 \
    --http_port=9080 \
    --https_port=9081 \
    "${WPR_FILE}" &> "${OUTPUT_DIR}"/wpr_scripts/wpr.log &

WPR_PID=$!

echo -n "WPR running in \"${WPR_MODE}\" mode with pid ${WPR_PID}"
# create a file which the CI job can write "kill" to kill wpr
echo "${WPR_MODE} ${WPR_FILE}" > "${OUTPUT_DIR}"/wpr_scripts/wpr_state
while ! grep -q kill "${OUTPUT_DIR}"/wpr_scripts/wpr_state
do
    echo -n "."
    ps -fp ${WPR_PID} > /dev/null
    sleep 5
done
echo killing > "${OUTPUT_DIR}"/wpr_scripts/wpr_state

# Gracefully stop wpr for it to write any output files
# Note: go spawns a child process which actually does the work
echo "Stopping WPR PID ${WPR_PID}"
pkill --signal INT --parent ${WPR_PID}
wait

# Delete the replay file if its not new.
# It's typically listed as a gitlab artifact so this saves space
if [ "${WPR_MODE}" = "replay" ] ; then
    rm -f "${WPR_FILE}"
else
    # its a new file and was created outside the OUTPUT_DIR
    # put it back so it can be archived
    echo "Moving: ${WPR_FILE} to ${NEW_WPR_FILE}"
    mv "${WPR_FILE}" "${NEW_WPR_FILE}"
fi

# Delete out state file so the main container knows wpr is stopped
rm -f "${OUTPUT_DIR}"/wpr_scripts/wpr_state
