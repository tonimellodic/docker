# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# this is a list of container structure tests which
# are automatically run as part of the CI process
# Docs: https://github.com/GoogleContainerTools/container-structure-test
schemaVersion: "2.0.0"

metadataTest:
  env:
    - key: "ANDROID_HOME"
      value: "/opt/android-sdk"
    - key: "ANDROID_NDK_ROOT"
      value: "/opt/android-ndk-r16b"
    - key: "ANDROID_NDK_HOME"
      value: "/opt/android-ndk-r16b"
  cmd:
    [
      "gitlab-runner",
      "run",
      "--working-directory",
      "/opt/ci",
      "--user",
      "ci_user",
    ]
  entrypoint: ["/usr/bin/dumb-init", "--"]

fileContentTests:
  - name: "gitlab runner prefs file"
    path: "/etc/apt/preferences.d/pin-gitlab-runner.pref"
    expectedContents: ["Package: gitlab-runner"]

  - name: "ci_user should be a sudoer"
    path: "/etc/sudoers"
    expectedContents: ['ci_user ALL=\(ALL\) NOPASSWD:ALL']

  - name: "make sure theres no gitlab token"
    path: "/etc/gitlab-runner/config.toml"
    excludedContents: ["token"]

# Warning - anything you ask to check will be read entirely
# into RAM, this includes whole directories.
fileExistenceTests:
  - name: "/opt/ci ownership"
    path: "/opt/ci"
    uid: 1000

  - name: "check android sdk is installed"
    path: "$ANDROID_HOME/.knownPackages"

  - name: "check android ndk is installed"
    path: "$ANDROID_NDK_HOME/ndk-build"

commandTests:
  - name: "gitlab-runner binary"
    command: "gitlab-runner"
    args: ["--version"]

  - name: "check gitlab user uid is 1000"
    command: "id"
    args: ["ci_user"]
    expectedOutput: ["uid=1000"]

  - name: "check sdkmanager version"
    command: "$ANDROID_HOME/tools/bin/sdkmanager"
    args: ["--version"]
    expectedOutput: ["26.1.1"]

  - name: "check android targets"
    command: "$ANDROID_HOME/tools/bin/avdmanager"
    args: ["list", "target"]
    expectedOutput: ["Name: Android API 28"]

  - name: "check dev tools are installed"
    command: "which"
    args:
      - make
      - java
      - npm
      - python
      - clang
      - clang++
