#!/bin/bash

# Copyright (c) 2021-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERBOSE=0

# Run a command, output depends on verbosity level
run() {
  if [ "$VERBOSE" -lt 0 ]; then
    VERBOSE=0
  fi
  if [ "$VERBOSE" -gt 1 ]; then
    echo "COMMAND: $@"
  fi
  case $VERBOSE in
  0)
    eval "$@" >/dev/null 2>&1
    ;;
  *)
    eval "$@"
    ;;
  esac
}

help() {
  echo ""
  echo "Usage: $0 -v"
  echo -e "\t-v | --verbose print emulator command logs"
}

while [ "$1" != "" ]; do
  case $1 in
  -v | --verbose)
    VERBOSE=1
    ;;
  -h | --help)
    help
    exit
    ;;
  *)
    help
    exit 1
    ;;
  esac
  shift
done

run emulator -avd "device-android-32" \
  -no-window \
  -skip-adb-auth \
  -no-snapshot \
  -wipe-data \
  -no-boot-anim \
  -gpu swiftshader_indirect &

adb start-server
echo "Wait for device"
timeout 30 adb wait-for-any-device
adb devices
