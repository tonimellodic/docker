# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:18.04

# Settings used later on in Dockerfile.
ARG WORK_DIR="/opt/ci"
ARG TIMEZONE="Europe/Berlin"

# Adjust env.
ENV LC_CTYPE="en_US.UTF-8"

# Set timezone and create our workdir
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
&&  echo $TIMEZONE > /etc/timezone \
&&  mkdir $WORK_DIR

# Install generic system packages
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
&&  dpkg --add-architecture i386 \
&&  apt-get update \
&&  apt-get install -qy --no-install-recommends \
        build-essential \
        ca-certificates \
        ccache \
        curl \
        dumb-init \
        git \
        libgtk2.0-dev \
        libc6:i386 \
        lsb-core \
        python \
        python3.6 \
        python3-requests \
        sudo \
        unzip \
        wget \
        xz-utils

# Install Android NDK to /third_party/
RUN mkdir /third_party/ \
&&  wget --quiet https://dl.google.com/android/repository/android-ndk-r16b-linux-x86_64.zip -O /tmp/android-ndk-16.zip \
&&  unzip -q /tmp/android-ndk-16.zip -d /third_party/ \
&&  rm /tmp/android-ndk-16.zip

RUN wget --quiet https://dl.google.com/android/repository/android-ndk-r20b-linux-x86_64.zip -O /tmp/android-ndk-20.zip \
&&  unzip -q /tmp/android-ndk-20.zip -d /third_party/ \
&&  rm /tmp/android-ndk-20.zip
