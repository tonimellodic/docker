# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
FROM ubuntu:18.04

# Settings used later on in Dockerfile.
ARG WORK_DIR="/opt/ci"
ARG TIMEZONE="Europe/Berlin"
ARG DEPOT_TOOLS="/opt/depot_tools"

# Adjust env
ENV GOOGLE_PLAY_AGREE_LICENSE="1" \
    LC_CTYPE="en_US.UTF-8" \
    CHROME_DEVEL_SANDBOX="1" \
    DEPOT_TOOLS="$DEPOT_TOOLS"

# Set timezone and create our workdir
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
&&  echo $TIMEZONE > /etc/timezone \
&&  mkdir $WORK_DIR

# Define some system-wide requirements that are needed
# for things other than the build to work
ARG SYSTEM_REQUIREMENTS="\
  build-essential \
  ca-certificates \
  ccache \
  curl \
  dumb-init \
  git \
  lsb-core \
  p7zip-full \
  python \
  sudo \
  unzip \
  wget \
"

#  Things which the actual build needs.
ARG BUILD_REQUIREMENTS="\
  clang \
  openjdk-8-jdk-headless \
  pkg-config \
  python-setuptools \
"

# Install generic system packages before chromium specific dependencies (below the line break)
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
&&  apt-get update \
&&  apt-get install -qy --no-install-recommends $SYSTEM_REQUIREMENTS $BUILD_REQUIREMENTS \
&&  rm -rf /var/lib/apt/lists/*

# Install Chromium's depot_tools.
RUN git clone --single-branch https://chromium.googlesource.com/chromium/tools/depot_tools.git $DEPOT_TOOLS

# Install Chromium build dependencies so less is installed at build time
# Installed deps are tracked using an md5sum of the script which is read at build time:
# https://chromium-gitlab.eyeo.it/eyeo/abpchromium/-/blob/6de80d163d2d3dc3a65a352b707d49845f6ae254/.gitlab-ci.yml#L44-53
ADD install-build-deps.sh install-build-deps-android.sh /tmp/build/
RUN /tmp/build/install-build-deps-android.sh \
&&  md5sum /tmp/build/* | sed -e 's|/tmp/||g' >> /installed_deps \
&&  rm -rf /var/lib/apt/lists/* /tmp/build
