# tox Docker Images

The [tox](https://tox.readthedocs.io/en/latest/) software is used to automate
testing in Python.
The [Docker](https://www.docker.com/) resources herein are used to assemble
Docker container images with the tox software pre-installed.
