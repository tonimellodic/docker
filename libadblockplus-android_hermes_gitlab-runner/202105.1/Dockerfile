# Copyright (c) 2021-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:20.04

# Settings used later on in Dockerfile.
ARG GRADLE_VERSION=6.7.1
ARG GRADLE_HASH=22449f5231796abd892c98b2a07c9ceebe4688d192cd2d6763f8e3bf8acbedeb
ARG CMDLINE_TOOLS_VERSION=7302050_latest
ARG CMDLINE_TOOLS_HASH=7a00faadc0864f78edd8f4908a629a46d622375cbe2e5814e82934aebecdb622
ARG NODE_VERSION=v14.17.0
ARG NODE_HASH=494b161759a3d19c70e3172d33ce1918dd8df9ad20d29d1652a8387a84e2d308
ARG WORK_DIR="/opt/ci"
ARG TIMEZONE="Europe/Berlin"

# Adjust env.
ENV DEBIAN_FRONTEND noninteractive
ENV LC_CTYPE "en_US.UTF-8"
ENV ANDROID_HOME /opt/android-sdk
ENV NPM_PATH /opt/node/bin/npm
ENV PATH $ANDROID_HOME/cmdline-tools/latest/bin:/opt/node/bin:/opt/gradle/bin:$PATH
ENV TZ "${TIMEZONE}"

# Create our workdir
RUN mkdir "$WORK_DIR"

RUN apt-get update \
&&  apt-get upgrade -y \
&&  apt-get install -qy --no-install-recommends \
        build-essential \
        curl \
        libicu-dev \
        ninja-build \
        openjdk-8-jdk-headless \
        python3 \
        python-is-python3 \
        tzdata \
        unzip \
        xz-utils \
        zip \
&&  apt-get clean \
&&  rm -rf /var/lib/apt/lists/*

# Install Gradle (Apache License 2.0)
RUN curl -o /tmp/gradle.zip -Ls "https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-all.zip" \
&&  echo "${GRADLE_HASH}  /tmp/gradle.zip" | sha256sum --check \
&&  unzip /tmp/gradle.zip -d /opt \
&&  rm -f /tmp/gradle.zip \
&&  mv "/opt/gradle-${GRADLE_VERSION}" /opt/gradle

# Install Android SDK & side-by-side NDK (License: https://developer.android.com/studio/terms)
RUN curl -o /tmp/commandlinetools.zip -Ls https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip \
&&  echo "${CMDLINE_TOOLS_HASH}  /tmp/commandlinetools.zip" | sha256sum --check \
&&  mkdir -p $ANDROID_HOME/cmdline-tools \
&&  unzip /tmp/commandlinetools.zip -d /opt \
&&  mv /opt/cmdline-tools /opt/android-sdk/cmdline-tools/latest \
&&  rm -f /tmp/commandlinetools.zip \
&&  yes | sdkmanager --licenses \
&&  sdkmanager --install \
        'build-tools;30.0.2' \
        'cmake;3.10.2.4988404' \
        'ndk;21.1.6352462' \
        'platform-tools' \
        'platforms;android-29'

# Install Node (License: https://raw.githubusercontent.com/nodejs/node/v14.17.0/LICENSE)
RUN curl -o /tmp/node.tar.xz -Ls "https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-linux-x64.tar.xz" \
&&  echo "${NODE_HASH}  /tmp/node.tar.xz" | sha256sum --check \
&&  tar xf /tmp/node.tar.xz -C /opt \
&&  rm -f /tmp/node.tar.xz \
&&  mv "/opt/node-${NODE_VERSION}-linux-x64" /opt/node

