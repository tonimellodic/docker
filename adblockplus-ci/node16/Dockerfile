# Copyright (c) 2019-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM debian:buster-slim

# reverse engineered from https://deb.nodesource.com/setup_10.x
ENV REPOSITORY https://deb.nodesource.com/node_16.x buster main
ENV SOURCELIST_FILE /etc/apt/sources.list.d/nodesource.list

RUN apt-get update  \
&&  apt-get install -y apt-transport-https wget gnupg

RUN echo "deb $REPOSITORY" > $SOURCELIST_FILE  \
&&  echo "deb-src $REPOSITORY" >> $SOURCELIST_FILE

RUN wget https://deb.nodesource.com/gpgkey/nodesource.gpg.key  \
&&  apt-key add nodesource.gpg.key 1>/dev/null 2>/dev/null  \
&&  apt-get update  \
&&  apt-get install -y git python-pip libgtk-3-0 libxt6 xvfb libnss3 libxss1  \
&&  apt-get install -y libgconf-2-4 libasound2 libgbm1 nodejs unzip

RUN pip install Jinja2 cryptography fonttools brotli

# fonttools executables are installed in ~/.local/bin
# see https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/115
ENV PATH="${PATH}:~/.local/bin"
